Made 2 Spring boot apis. 
First one uses Sprinboot to perform CRUD operations on Employee data 
with mongoDB as database in the backend and redis cache to retrieve frequently used data.

Second one shows the working of rabbitmq using Spring Boot and Spring Cloud Stream.